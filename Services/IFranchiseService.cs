﻿using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchises>> GetAllFranchisesAsync();
        public Task<Franchises> GetSpecificFranchiseAsync(int id);
        public Task<Franchises> AddFranchiseAsync(Franchises franchise);
        public Task UpdateFranchiseAsync(Franchises franchise);
        public Task<bool> UpdateFranchiseMoviesAsync(int id, int[] movieIds);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);

    }
}

