﻿using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly CharacterDbContext _context;
        public FranchiseService(CharacterDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all Franchises
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchises>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
             .Include(f => f.Movies)
             .ToListAsync();
        }
        /// <summary>
        /// Get One Franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Franchises> GetSpecificFranchiseAsync(int id)
        {
            Franchises franchise = await _context.Franchises
                           .Include(f => f.Movies)
                           .Where(f => f.Id == id)
                           .FirstAsync();
            return franchise;
        }
        /// <summary>
        /// post a Franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task<Franchises> AddFranchiseAsync(Franchises franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }
        /// <summary>
        /// update Franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchises franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// checks whether franchise exits
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }
        /// <summary>
        ///  delete a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// update Franchises
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task UpdateFranchisesAsync(Franchises franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// updating movies in franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        public async Task<bool> UpdateFranchiseMoviesAsync(int id, int[] movieIds)
        {
            foreach (int movieId in movieIds)
            {
                Movies movieToUpdate = await _context.Movies.FindAsync(movieId);

                if (movieToUpdate == null)
                {
                    return false;
                }
                movieToUpdate.FranchiseId = id;
            }
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
