﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Characters;
using MovieCharecters_API.Services;

namespace MovieCharecters_API.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly CharacterDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;
        public CharactersController( CharacterDbContext context, IMapper mapper, ICharacterService characterService)
        {
            _context = context;
            _characterService = characterService;
            _mapper = mapper;
        }
        // GET: api/Characters
        /// <summary>
        /// get all characters 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            //return await _context.Characters.ToListAsync();
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }
       // GET: api/Characters/5
       /// <summary>
       /// get character by Id
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacters(int id)
        {
            var characters = await _characterService.GetSpecificCharacterAsync(id);
            if (characters == null)
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(characters);
        }
        // PUT: api/Characters/5
        /// <summary>
        /// update character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacters(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            Characters domainCharacter = _mapper.Map<Characters>(dtoCharacter);
            await _characterService.UpdateCharacterAsync(domainCharacter);
            return NoContent();
        }
        // POST: api/Characters
        /// <summary>
        /// post character
        /// </summary>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Characters>> PostCharacters(CharacterCreateDTO dtoCharacter)
        {
            Characters domainCharacter = _mapper.Map<Characters>(dtoCharacter);
            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);
            return CreatedAtAction("GetCharacters",
                new { CharacterId = domainCharacter.Id },
                 _mapper.Map<CharacterEditDTO>(domainCharacter));
        }
        /// <summary>
        /// delete character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CharectersExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacters(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacterAsync(id);
            return NoContent();
        }
        
    }
}
