﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Movies;
using MovieCharecters_API.Services;

namespace MovieCharecters_API.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly CharacterDbContext _context;
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;
        public MoviesController(CharacterDbContext context, IMovieService movieService, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
        }
        // GET: api/Movies
        /// <summary>
        /// get all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }
    // GET: api/Movies/5
    /// <summary>
    /// get movie by Id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovies(int id)
        {
            var movies = await _movieService.GetSpecificMovieAsync(id);
            if (movies == null)
            {
                return NotFound();
            }
            return _mapper.Map<MovieReadDTO>(movies);
        }
        // PUT: api/Movies/5
        /// <summary>
        /// update a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovies"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovies(int id, MovieEditDTO dtoMovies)
        {
            if (id != dtoMovies.Id)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            Movies domainMovie = _mapper.Map<Movies>(dtoMovies);
            await _movieService.UpdateMovieAsync(domainMovie);
            return NoContent();
        }
        // POST: api/Movies
        /// <summary>
        /// post a movie
        /// </summary>
        /// <param name="dtoMovies"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movies>> PostMovies(MovieCreateDTO dtoMovies)
        {
            Movies domainMovie = _mapper.Map<Movies>(dtoMovies);
            domainMovie = await _movieService.AddMovieAsync(domainMovie);
            return CreatedAtAction("GetMovies", 
                   new { MovieId = domainMovie.Id },
                  _mapper.Map<MovieCreateDTO>(dtoMovies));
        }
        //DELETE: api/Movies/5
        /// <summary>
        /// delete a Movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovies(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(id);
            return NoContent();
        }
        private bool MoviesExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
        /// <summary>
        /// Update characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="charactrers"></param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, List<int> charactrers)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            try
            {
                await _movieService.UpdateCharactersInMovieAsync(id, charactrers);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalied character.");
            }
            return NoContent();
        }

    }
}
