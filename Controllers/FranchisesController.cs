﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Franchises;
using MovieCharecters_API.Services;

namespace MovieCharecters_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly CharacterDbContext _context;
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;
        public FranchisesController(CharacterDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;
        }
        // GET: api/Franchises
        /// <summary>
        /// get all franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }
        // GET: api/Franchises/5
        /// <summary>
        /// get franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchises(int id)
        {
            var franchises = await _context.Franchises.FindAsync(id);
            if (franchises == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseReadDTO>(franchises);
        }
        // PUT: api/Franchises/5
        /// <summary>
        /// update franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchises"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchises(int id, FranchiseEditDTO dtoFranchises)
        {
            if (id != dtoFranchises.Id)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            Franchises domainFranchise = _mapper.Map<Franchises>(dtoFranchises);
            await _franchiseService.AddFranchiseAsync(domainFranchise);
            return NoContent();
        }
        // POST: api/Franchises
       /// <summary>
       /// post franchise
       /// </summary>
       /// <param name="dtoFranchises"></param>
       /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchises>> PostFranchises(FranchiseCreateDTO dtoFranchises)
        {
            Franchises domainFranchise = _mapper.Map<Franchises>(dtoFranchises);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);
            return CreatedAtAction("GetFranchises", new { FranchiseId = domainFranchise.Id },
                  _mapper.Map<FranchiseCreateDTO>(dtoFranchises));
        }
        // DELETE: api/Franchises/5
        /// <summary>
        /// delete franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchises(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchiseAsync(id);
            return NoContent();
        }
        private bool FranchisesExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
        /// <summary>
        ///  update movies in franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMovieFranchises(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateFranchiseMoviesrAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalied Movie.");
            }
            return NoContent();
        }
    }
}
