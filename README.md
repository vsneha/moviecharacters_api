# MovieCharacters_API

An Entity Frameword Code First workflow and an ASP.Net Core Web API in C#

## Getting started

Make sure you have installed at least the following tools.
- Visual Studio 2019 with .Net 5.
- SQL Server Management Studio


## Git Clone 
```
cd existing_repo
git remote add origin https://gitlab.com/venkatsetty.neha/moviecharacters_api.git
git branch -M main
git push -uf origin main
```

Technologies

C#, ASP.Net Core, Entity Framework,REST API,SQL Server,Swagger documentation

