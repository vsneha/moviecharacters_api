﻿using AutoMapper;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Characters, CharacterReadDTO>()
                .ForMember(cdto => cdto.MovieId, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToArray()))
                .ForMember(cdto => cdto.MovieName, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Title).ToArray()))
                .ReverseMap();

            CreateMap<Characters, CharacterCreateDTO>().ReverseMap();

            CreateMap<Characters, CharacterEditDTO>().ReverseMap();


        }

    }
}
