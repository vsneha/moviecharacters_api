﻿using AutoMapper;
using MovieCharecters_API.Models.Domain;
using MovieCharecters_API.Models.DTO.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movies, MovieReadDTO>()
             .ForMember(mdto => mdto.Characters, opt => opt
             .MapFrom(m => m.Characters.Select(m => m.Id).ToArray()))
             .ForMember(mdto => mdto.FranchiseId, opt => opt
             .MapFrom(m => m.FranchiseId))
             .ReverseMap();

            CreateMap<Movies,MovieCreateDTO>()
             .ForMember(mdto => mdto.FranchiseId, opt => opt
             .MapFrom(m => m.FranchiseId))
             .ReverseMap();

            CreateMap<Movies, MovieEditDTO>()
             .ForMember(mdto => mdto.FranchiseId, opt => opt
             .MapFrom(m => m.FranchiseId))
             .ReverseMap();
        }

    }
}

