﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models.Domain
{
    [Table("Characters")]
    public class Characters
    {
        //PK
        public int Id { get; set; }
        //Fields
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(50)]
        public string Gender { get; set; }
        public string Picture { get; set; }
        // Relationships many to many
        public ICollection<Movies> Movies { get; set; }


    }
}


