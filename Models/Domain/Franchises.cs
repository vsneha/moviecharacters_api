﻿   using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models.Domain
{ 
    [Table("Franchises")]
    public class Franchises
    {   
        //PK
        public int Id { get; set; }
        //Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        //Relationships
        public ICollection<Movies> Movies { get; set; }
    }
}
