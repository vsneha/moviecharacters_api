﻿using Microsoft.EntityFrameworkCore;
using MovieCharecters_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models
{
    public class CharacterDbContext : DbContext

    {
        public CharacterDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movies> Movies { get; set; }
        public DbSet<Characters> Characters { get; set; }
        public DbSet<Franchises> Franchises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchises>().HasData(new Franchises { Id = 1, Name = " Geetha Arts", Description = " The company is known for its big-budget films" });
            modelBuilder.Entity<Franchises>().HasData(new Franchises { Id = 2, Name = "Dil Raju", Description = "The company has consistently delivered many blockbuster movies" });


            modelBuilder.Entity<Characters>().HasData(new Characters { Id = 1, FullName = "Prabas", Alias = "Mr.Perfect", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Baahubali:_The_Beginning" });
            modelBuilder.Entity<Characters>().HasData(new Characters { Id = 2, FullName = "Rajini Kanth", Alias = "Super Star", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Enthiran" });
            modelBuilder.Entity<Characters>().HasData(new Characters { Id = 3, FullName = "Allu Arjun", Alias = "Stylish Star", Gender = "Male", Picture = "https://en.wikipedia.org/wiki/Julayi" });

            modelBuilder.Entity<Movies>().HasData(new Movies { Id = 1, Title = "Bahubali", Genre = "Fantacy", ReleaseYear = 2015, Director = "Rajamouli", FranchiseId = 1, Picture = "https://en.wikipedia.org/wiki/Baahubali:_The_Beginning", Trailer = "https://www.youtube.com/watch?v=sOEg_YZQsTI" });
            modelBuilder.Entity<Movies>().HasData(new Movies { Id = 2, Title = "Robo", Genre = "Fiction", ReleaseYear = 2010, Director = "Shanker", FranchiseId = 2, Picture = "https://en.wikipedia.org/wiki/Enthiran", Trailer = "https://www.youtube.com/watch?v=1jgNECZtv5Q" });
            modelBuilder.Entity<Movies>().HasData(new Movies { Id = 3, Title = "Julayi", Genre = "Comedy",ReleaseYear = 2012, Director = "Trivikram", FranchiseId = 1 , Picture = "https://en.wikipedia.org/wiki/Julayi", Trailer = "https://www.youtube.com/watch?v=w3h7QwajjKs" });

            // Seeding m2m Movies-Characters. Access to linking table
            modelBuilder.Entity<Characters>()
                .HasMany(x => x.Movies)
                .WithMany(y => y.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharaterMovie",
                p => p.HasOne<Movies>().WithMany().HasForeignKey("MovieId"),
                m => m.HasOne<Characters>().WithMany().HasForeignKey("CharacterId"),
                n =>
                {
                    n.HasKey("CharacterId", "MovieId");
                    n.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 2, CharacterId = 2 },
                        new { MovieId = 3, CharacterId = 3 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 2, CharacterId = 1 },
                        new { MovieId = 3, CharacterId = 1 }
                       );
                });
        }
    }
}


  


