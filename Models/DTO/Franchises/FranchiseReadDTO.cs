﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models.DTO.Franchises
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> MovieId { get; set; }
        public List<string> MovieName { get; set; }
    }
}
