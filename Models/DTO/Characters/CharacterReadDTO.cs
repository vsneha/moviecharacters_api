﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharecters_API.Models.DTO.Characters
{
    public class CharacterReadDTO
    {   public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<int> MovieId { get; set; }
        public List<string> MovieName { get; set; }

    }
}
